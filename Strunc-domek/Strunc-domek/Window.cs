﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strunc_domek
{
    public  class Window : BaseComponent
    {
        public void draw(Graphics g)
        {
            SolidBrush BlueBrush = new SolidBrush(Color.LightBlue);

         
            g.FillRectangle(BlueBrush, X - Sirka / 2, Y - Vyska / 2, Sirka, Vyska);
            g.DrawRectangle(Pens.Black, X - Sirka / 2, Y - Vyska / 2, Sirka, Vyska);
            g.DrawLine(Pens.Black, X, Y-Vyska/2, X, Y + Vyska/2);
            g.DrawLine(Pens.Black, X-Sirka/2, Y, X+Sirka/2, Y);
        }
    }
}
