﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strunc_domek
{
    public abstract class BaseComponent
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }
        public int Size { get; set; }
        public int Sirka { get; set; }
        public int Vyska { get; set; }
    }
}
