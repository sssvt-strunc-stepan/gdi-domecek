﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strunc_domek
{
    public class House : BaseComponent
    {
        public void draw(Graphics g)
        {

            SolidBrush solidBrush = new SolidBrush(Color.DarkGray);
            g.FillRectangle(solidBrush, X, Y, Sirka, Vyska);
            g.DrawRectangle(Pens.Black, X, Y, Sirka, Vyska);
          //  g.fillTriangle(solidBrush)

            
            g.DrawLine(Pens.Black, X, Y, X+Sirka/2, Y-Vyska/4);
            g.DrawLine(Pens.Black, X + Sirka / 2, Y - Vyska / 4, X + Sirka, Y);

        }
    }
}
