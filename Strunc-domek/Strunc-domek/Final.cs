﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Strunc_domek
{
    internal class Final
    {

        Graphics G;
        private int Doorbase;
        private int deleno2;

        private int SirkaDomu;
        private int VyskaDomu;
        private int XDomu;
        private int YDomu;
        public Final(Graphics g)
        {
            G = g;
        }
        public void draw()
        {

            
            drawHouse();
            drawDoor();
            drawWindow();
            DrawSun();
            DrawFence();
            


        }

        public void drawHouse()
        {
            House house = new House()
            {
                X = 250,
                Y = 200,
                Sirka = 240,
                Vyska = 280
            };
            SirkaDomu = house.Sirka;
            VyskaDomu = house.Vyska;
            XDomu = house.X;
            YDomu = house.Y;


            Doorbase = house.Vyska + house.Y;
            deleno2 = house.X + house.Sirka / 2;

            house.draw(G);
        }

           public void drawDoor()
        {

            int sirka = 60;
            int vyska = 100;
            Door door = new Door()
            {
                Y = Doorbase - vyska,
                X = deleno2 - sirka / 2,
                Sirka = sirka,
                Vyska = vyska


            };
            door.draw(G);
            





        }
        public void DrawFence()
        {
            int x = XDomu;
            int y = YDomu + VyskaDomu;
 


            /* horní část*/
            plotTop railaTop = new plotTop()
            {
                Y = YDomu + VyskaDomu * 8 / 10,
                X = XDomu,
                Y2 = YDomu + VyskaDomu * 8 / 10,
                X2 = 0
            };
            railaTop.draw(G);


            /* prostřední část*/
            plotMid railMid = new plotMid()
            {
                Y = YDomu + VyskaDomu * 9 / 10,
                X = XDomu,
                Y2 = YDomu + VyskaDomu * 9 / 10,
                X2 = 0
            };
            railMid.draw(G);

            /* dolní část */
            plotDown railDown = new plotDown()
            {
                Y = YDomu + VyskaDomu * 10 / 10,
                X = XDomu,
                Y2 = YDomu + VyskaDomu * 10 / 10,
                X2 = 0
            };
            railDown.draw(G);

            for (int i = 1; i <= 10; i++)
            {
                plotPole pole = new plotPole()
                {
                    X = XDomu - XDomu / 10 * i,
                    Y = YDomu + VyskaDomu,
                    X2 = XDomu - XDomu / 10 * i,
                    Y2 = YDomu + VyskaDomu * 7 / 10

                };

                pole.draw(G);
            }
        }

        public void drawWindow()
        {
            Window window = new Window()
            {
                X = XDomu + SirkaDomu / 3,
                Y = YDomu + VyskaDomu / 3,
                Sirka = 40,
                Vyska = 40
            };
            window.draw(G);




            Window window2 = new Window()
            {
                X = XDomu + SirkaDomu * 2 / 3,
                Y = YDomu + VyskaDomu / 3,
                Sirka = 40,
                Vyska = 40
            };
            window2.draw(G);


/*
            Window window3 = new Window()
            {
                X = XDomu + SirkaDomu * 1 / 4,
                Y = YDomu + VyskaDomu / 4,
                Sirka = 40,
                Vyska = 40
            };
            window3.draw(G);
        
            */
            
        }



        public void DrawSun()
        {
            Sun sun = new Sun()
            {
                X =  100  ,
                Y =  80,
                Sirka = 60,
                Vyska = 60
            };
            sun.draw(G);
        }


    }
}
